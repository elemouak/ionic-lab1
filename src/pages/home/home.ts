import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailPage } from '../detail/detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  produits: any[];

  constructor(public navCtrl: NavController) {
    this.produits = [
      {id: '1', name: 'Produit1', image: 'assets/imgs/m1.jpg'},
      {id: '2', name: 'Produit2', image: 'assets/imgs/m2.jpg'},
      {id: '3', name: 'Produit3', image: 'assets/imgs/m3.jpg'},
      {id: '4', name: 'Produit4', image: 'assets/imgs/m4.jpg'},
      {id: '5', name: 'Produit5', image: 'assets/imgs/m5.jpg'}
    ]
  }

  showDetail(item){
    console.log(item);
    this.navCtrl.push(DetailPage, {item: item});
  }

}
